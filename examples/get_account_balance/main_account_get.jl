using Printf
using PyCall
pushfirst!(PyVector(pyimport("sys")."path"), "../")
br = pyimport("binance_request")
binance = br.SignedBinanceRequest(ARGS[1])

urlUserData = "/api/v3/account"
params = Dict()
println("[printAccountBalance] Sending request...")
result = binance.makeRequest(urlUserData, params)

balances = result["balances"]
for balance in balances
    asset = balance["asset"]
    free = parse(Float64, balance["free"])
    locked = parse(Float64, balance["locked"])
    if free == 0 && locked == 0
        continue
    end
    @printf("Asset: %s. Free: %2.5f. Locked: %2.5f\n", asset, free, locked)
end