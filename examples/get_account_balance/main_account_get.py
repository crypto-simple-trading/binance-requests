import sys
from binance_requests import BinanceClient

def printAccountBalance(client:BinanceClient):
    print("[printAccountBalance] Sending request...")
    result = client.getAccountBalance()
    totalBusd = 0
    print("[printAccountBalance] Result:")
    for coin, values in result["balance"].items():
        free, locked, busdValue = values
        if free == 0 and locked == 0:
            continue
        totalBusd += busdValue
        print(f" - Asset: {coin}. Free: {free:2.5f}. Locked: {locked:2.5f}. BUSD equivalent: {busdValue:2.5f}")
    print("=========")
    print(f"Total BUSD: {totalBusd:2.5f}")

def main():
    client = BinanceClient(sys.argv[1])
    printAccountBalance(client)

if __name__ == "__main__":
    main()